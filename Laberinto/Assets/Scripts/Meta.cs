﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meta : MonoBehaviour
{
    Canvas canvas3;
    public bool meta = false;

    private void Start()
    {
        canvas3 = GameObject.Find("Canvas3").GetComponent<Canvas>();
        canvas3.enabled = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") {
            meta = true;
            canvas3.enabled = true;
        }
    }
}
